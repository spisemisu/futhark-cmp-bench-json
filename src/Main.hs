#!/usr/bin/env stack
{- stack
   --resolver lts-13.19
   --install-ghc
   script
   --package json
   --ghc-options -Werror
   --ghc-options -Wall
   --
-}

--------------------------------------------------------------------------------
--
-- (c) 2019 SPISE MISU ApS, opensource.org/licenses/ISC
--
--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import           Data.List
  ( intersectBy
  , sort
  )
import           System.Environment
  ( getArgs
  )
import           Text.JSON
import           Text.JSON.Types
import           Text.Printf
  ( printf
  )

--------------------------------------------------------------------------------

-- Rewrite cmp-bench-json.py in Haskell #748
--
-- https://github.com/diku-dk/futhark/issues/748

-- Index of /benchmark-results/
--
-- https://futhark-lang.org/benchmark-results/

-- Look at current code:
--
-- https://github.com/diku-dk/futhark/blob/master/src/Futhark/CLI/Bench.hs#L66

--------------------------------------------------------------------------------

-- OUTPUT

data Colors
  = HEADER
  | OKBLUE
  | OKGREEN
  | WARNING
  | FAIL
  | ENDC
  | BOLD
  | UNDERLINE

instance Show Colors where
  show HEADER    = "\ESC[95m"
  show OKBLUE    = "\ESC[94m"
  show OKGREEN   = "\ESC[92m"
  show WARNING   = "\ESC[93m"
  show FAIL      = "\ESC[91m"
  show ENDC      = "\ESC[00m"
  show BOLD      = "\ESC[01m"
  show UNDERLINE = "\ESC[04m"

--------------------------------------------------------------------------------

-- JSON SAMPLE

{-
{
  "futhark-benchmarks/misc/life/life.fut": {
    "datasets": {
      "#1 (\"[[0i32, 0i32, 0i32, 0i32, 0i32], [0i32, 0i32, 1i32...\")": {
        "stderr": "\"\"",
        "runtimes": [
          233,
          142,
          142,
          138,
          130,
          144,
          143,
          133,
          128,
          134
        ]
      }
    }
  },
  "futhark-benchmarks/rodinia/bfs/bfs_asympt_ok_but_slow.fut": {
    "datasets": {
      "data/64kn_32e-var-1-256-skew.in": {
        "stderr": "\"\"",
        "runtimes": [
          47714,
          47763,
          47721,
          49880,
          45493,
          43812,
          43864,
          43887,
          42579,
          42537
        ]
      },
      "data/512nodes_high_edge_variance.in": {
        "stderr": "\"\"",
        "runtimes": [
          3455,
          3322,
          3327,
          3299,
          3296,
          3310,
          3282,
          3398,
          3353,
          3291
        ]
      },
      "data/graph1MW_6.in": {
        "stderr": "\"\"",
        "runtimes": [
          35040,
          34900,
          34915,
          34771,
          34865,
          34762,
          35068,
          34865,
          35092,
          35351
        ]
      },
      "data/4096nodes.in": {
        "stderr": "\"\"",
        "runtimes": [
          6904,
          6812,
          6646,
          6696,
          6755,
          6827,
          6732,
          6741,
          6757,
          6791
        ]
      }
    }
  },
  ...
}
-}

--------------------------------------------------------------------------------

-- PARSE

data Benchmark = Benchmark
  { benchmark :: [ FutProg ]
  } deriving Show

data FutProg = FutProg
  { filename :: String
  , datasets :: [ Dataset ]
  } deriving (Eq, Ord, Show)

data Dataset = Dataset
  { input :: String
  , error :: Maybe String
  , times :: [ Double ]
  } deriving (Eq, Ord, Show)

instance JSON Benchmark where
  readJSON =
    ben
    where
      jtl (JSNull         ) = [            ]
      jtl (JSBool     _   ) = [            ]
      jtl (JSRational _ _ ) = [            ]
      jtl (JSString   _   ) = [            ]
      jtl (JSArray    _   ) = [            ]
      jtl (JSObject   x   ) = fromJSObject x
      num (JSNull         ) = 0
      num (JSBool     _   ) = 0
      num (JSRational _ r ) = fromRational r
      num (JSString   _   ) = 0
      num (JSArray    _   ) = 0
      num (JSObject   _   ) = 0
      ben obj =
        (
          fut $ jtl obj
        ) >>= Ok . Benchmark
      fut xs =
        (
          sequence $
          map
          (
            \(x, obj) ->
              let
                ys = jtl obj
              in
                bef ys >>= \ y ->
                Ok $ FutProg x y
          )
          xs
        ) >>= Ok . sort
      bef xs =
        (
          case filter ((== "datasets") . fst) $ xs of
            [ ] -> Error msg
            h:_ -> dat $ jtl $ snd h
        ) >>= Ok
        where
          msg = "No `datasets` are present"
      dat xs =
        (
          sequence $
          map
          (
            \(x, obj) ->
              let
                ys = jtl obj
              in
                err ys >>= \ y  ->
                run ys >>= \ zs ->
                Ok $ Dataset x y zs
          )
          xs
        ) >>= Ok . sort
      err xs =
        (
          case filter ((== "stderr") . fst) $ xs of
            [("stderr", JSString (JSONString ""))] -> Ok $ Nothing
            [("stderr", JSString (JSONString cs))] -> Ok $ Just cs
            ______________________________________ -> Error msg
        ) >>= Ok
        where
          msg = "No `stderr` is present"
      run xs =
        (
          case filter ((== "runtimes") . fst) $ xs of
            [("runtimes", JSArray ys)] -> Ok $ map num ys
            __________________________ -> Error msg
        ) >>= Ok
        where
          msg = "No `runtimes` are present"
  showJSON = undefined -- Not needed

--------------------------------------------------------------------------------

-- MISSING

type JsonFile = String

complain :: [ (JsonFile, Result Benchmark) ] -> [ String ]
complain =
  aux
  where
    aux [                      ] = []
    aux ((f1, bs1):(f2, bs2):[]) =
      case (bs1, bs2) of
        (Error e1, Error e2) -> [ err f1 e1 , err f2 e2 ]
        (Error e1, ________) -> [ err f1 e1 ]
        (________, Error e2) -> [ err f2 e2 ]
        (Ok    b1, Ok    b2) ->
          let
            ps =
              (
                map (\ x ->  mis f2 f1 $ filename x) $
                complementBy hlp (benchmark b2) (benchmark b1)
              ) ++
              (
                map (\ x ->  mis f1 f2 $ filename x) $
                complementBy hlp (benchmark b1) (benchmark b2)
              )
              where
                hlp x y = filename x /= filename y
            ds =
              map (\ x -> dat f1 f2 (filename x) (inp x)) $
              complementBy hlp (benchmark b1) (benchmark b2)
              where
                inp = concat . map input . datasets
                hlp x y = (filename x, inp x) /= (filename y, inp y)
            rs =
             -- TODO: Are runtimes not always present in JSON files? I mean they
             -- are arrays, so an empty array should always be the case!
              []
          in
            sort $ ps ++ ds ++ rs
    aux ________________________ = []
    err a b =
      "Error from parsing " ++ a ++ ": " ++ b
    mis a b c =
      "In " ++ a ++ " but not " ++ b ++ ": program " ++ c
    dat a b c d =
      mis a b c ++ " dataset " ++ d

--------------------------------------------------------------------------------

-- BENCHMARKS

type Significant = Bool
type InputData = String
type Latency = Double

data Speedup = Speedup
  { futapp :: String
  , inputs :: [ (Significant, InputData, Latency) ]
  }

speedups :: [ Result Benchmark ] -> [ Speedup ]
speedups =
  ben
  where
    ben [          ] = []
    ben (bs1:bs2:[]) =
      case (bs1, bs2) of
        (Ok b1, Ok b2) ->
          let
            xs = benchmark b1
            ys = benchmark b2
            fn = \ x y -> filename x == filename y
            zs = zip (intersectBy fn xs ys) (intersectBy fn ys xs)
          in
            map
            (
              \ (x, y) ->
                Speedup (filename x) $
                dat (datasets x) (datasets y)
            ) zs
        ______________ -> []
    ben ____________ = []
    dat xs ys =
      map
      (
        \ (x, y) ->
          let
            rt1 = times x
            rt2 = times y
            mn1 = average rt1
            mn2 = average rt2
            spd = latency mn1 mn2
            dif = abs $ mn1 - mn2
            sig = dif > (deviation rt1) / 2.0 + (deviation rt2) / 2.0
          in
            (sig, input x, dec spd 2)
      ) zs
      where
        fn = \ x y -> input x == input y
        zs = zip (intersectBy fn xs ys) (intersectBy fn ys xs)
    dec x n = y / z
      where
        y = fromIntegral $ ((round $ x * z) :: Integer)
        z = 10.0 ^ (n :: Integer)

--------------------------------------------------------------------------------

-- HELPERS

complementBy :: (a -> a -> Bool) -> [a] -> [a] -> [a]
complementBy eq xs ys =
  -- https://en.wikipedia.org/wiki/Set_(mathematics)#Complements
  [ x | x <- xs, all (eq x) ys ]

average :: [ Double ] -> Double
average xs =
  -- https://en.wikipedia.org/wiki/Average#Arithmetic_mean
  x / n
  where
    n = fromIntegral $ length xs
    x =                sum    xs

latency :: Double -> Double -> Double
latency =
  -- https://en.wikipedia.org/wiki/Speedup#Speedup_in_latency
  (/)

deviation :: [ Double ] -> Double
deviation xs =
  -- https://en.wikipedia.org/wiki/Standard_deviation
  sqrt $ s / n
  where
    n = fromIntegral $ length xs
    u = average xs
    s = sum $ map (\ x -> (x - u) ^ (2 :: Word)) xs

--------------------------------------------------------------------------------

main :: IO ()
main =
  getArgs          >>= \ fs ->
  mapM readFile fs >>= \ js ->
  let
    bs = map decode js
    xs = zip fs     bs
  in
    -- First we iterate through json_a and complain if we find something that is
    -- not in json _b.  Then we iterate through json_b and complain about
    -- everything that is not in json_a
    com xs >>
    -- Then we iterate through json_a and add to speedups everything that has a
    -- matching entry in json_b
    spd bs
  where
    com   = mapM_ putStrLn . complain
    spd   = mapM_ aux      . speedups
    aux x =
      -- We print Futhark program
      ps >>
      -- and then we print speedups for each dataset
      ds
      where
        ps = putStrLn $ printf "\n%s%s%s" (show HEADER) (futapp x) (show ENDC)
        ds =
          mapM_
          (
            \ (s,i,l) ->
              let
                c
                  | s && l > 1.01 = show OKGREEN
                  | s && l < 0.99 = show FAIL
                  | otherwise     = ""
              in
                putStrLn $
                printf "  %s%s%10.2fx%s" (pad ' ' 64 i) c l (show ENDC)
          ) $ inputs x
          where
            pad c n xs =
              ys ++ replicate (n - length ys) c
              where
                ys = take n xs
