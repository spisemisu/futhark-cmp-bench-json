#!/usr/bin/env bash

################################################################################
##
## (c) 2019 SPISE MISU ApS, opensource.org/licenses/ISC
##
################################################################################

clear

app="cmp-bench-json"

bin="$(stack path --local-install-root)/bin"

echo "### Clearing binary files:"
find  $bin -name $app -delete -print
find ./bin -name $app -delete -print
echo

echo "### Stack cleaning and building:" 
stack clean
#stack build --verbosity debug
#stack build --color=never
stack build --color=always
echo

echo "### Copying binary to local ./bin:" 
if [ ! -d ./bin ]; then
  mkdir -p ./bin;
fi
cp -v $bin/$app ./bin/
echo

echo "### Clearing all .cabal files:" 
find . -name "*.cabal" -delete -print
echo

echo -e "### Repoducible hash:\n-" $(sha256sum $bin/$app | cut -d " " -f 1)
echo
