futhark-cmp-bench-json
======================

Rewrite `cmp-bench-json.py` in Haskell #748:
- https://github.com/diku-dk/futhark/issues/748

# Usage

Run as a `script`:

```bash
./src/./Main.hs \
    dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json \
    dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json
```

Run as a `binary` (with pre-build):

```bash
build.bash
./bin/cmp-bench-json \
    dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json \
    dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json
```

# Ouput

Both `script` and `binary` will produce the following output, with `terminal`
colors, based on the sample files that can be found in the `dat` folder:

```bash
In dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json but not dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json: program futhark-benchmarks/misc/life/life.fut dataset #1 ("[[0i32, 0i32, 0i32, 0i32, 0i32], [0i32, 0i32, 1i32...")
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce-segmented.fut:prod_mat4_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce-segmented.fut:sum_i16
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce-segmented.fut:sum_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce-segmented.fut:sum_i64
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce-segmented.fut:sum_i8
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce-segmented.fut:sum_iota_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:lss_f32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:lss_f64
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:lss_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:lss_i8
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:lss_iota_f32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:lss_iota_f64
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:lss_iota_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:lss_iota_i8
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:prod_iota_mat4_f32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:prod_iota_mat4_f64
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:prod_iota_mat4_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:prod_iota_mat4_i8
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:prod_mat4_f32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:prod_mat4_f64
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:prod_mat4_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:prod_mat4_i8
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:sum_f32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:sum_f64
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:sum_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:sum_i8
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:sum_iota_f32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:sum_iota_f64
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:sum_iota_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:sum_iota_i8
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:sum_scaled_f32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:sum_scaled_f64
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:sum_scaled_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce.fut:sum_scaled_i8
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce_by_index.fut:absmax_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce_by_index.fut:sum_f32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce_by_index.fut:sum_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce_by_index.fut:sum_i32_f32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/reduce_by_index.fut:sum_vec_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan-segmented.fut:sum_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan-segmented.fut:sum_iota_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:lss_f32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:lss_f64
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:lss_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:lss_i8
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:lss_iota_f32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:lss_iota_f64
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:lss_iota_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:lss_iota_i8
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:prod_iota_mat4_f32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:prod_iota_mat4_f64
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:prod_iota_mat4_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:prod_iota_mat4_i8
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:prod_mat4_f32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:prod_mat4_f64
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:prod_mat4_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:prod_mat4_i8
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:sum_f32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:sum_f64
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:sum_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:sum_i8
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:sum_iota_f32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:sum_iota_f64
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:sum_iota_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:sum_iota_i8
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:sum_scaled_f32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:sum_scaled_f64
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:sum_scaled_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/scan.fut:sum_scaled_i8
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/transpose.fut:map_transpose_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/transpose.fut:map_transpose_i64
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/transpose.fut:map_transpose_i8
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/transpose.fut:transpose_i32
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/transpose.fut:transpose_i64
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/micro/transpose.fut:transpose_i8
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/misc/bfast/bfast-cloudy.fut
In dat/futhark-csopencl-GTX780-00355961da5eb9d89ba9db6cc7956515fb7a9a1b.json but not dat/futhark-csopencl-GTX780-00100ddfd7ab90d63c3e309e1e98c1ea32993d5c.json: program futhark-benchmarks/parboil/histo/histo.fut

futhark-benchmarks/Sune-ImageProc/interp.fut
  data/fake.in                                                          1.00x

futhark-benchmarks/Sune-ImageProc/interp_cos_plays.fut
  data/fake.in                                                          1.00x

futhark-benchmarks/accelerate/canny/canny.fut
  data/lena256.in                                                       1.08x
  data/lena512.in                                                       1.02x

futhark-benchmarks/accelerate/crystal/crystal.fut
  #1 ("200i32 30.0f32 5i32 1i32 1.0f32")                                1.01x
  #5 ("2000i32 30.0f32 50i32 1i32 1.0f32")                              1.00x
  #6 ("4000i32 30.0f32 50i32 1i32 1.0f32")                              1.00x

futhark-benchmarks/accelerate/fft/fft.fut
  data/1024x1024.in                                                     1.06x
  data/128x128.in                                                       0.91x
  data/128x512.in                                                       0.99x
  data/256x256.in                                                       1.00x
  data/512x512.in                                                       1.05x
  data/64x256.in                                                        0.99x

futhark-benchmarks/accelerate/fluid/fluid.fut
  benchmarking/medium.in                                                0.98x

futhark-benchmarks/accelerate/hashcat/hashcat.fut
  rockyou.dataset                                                       0.97x

futhark-benchmarks/accelerate/kmeans/kmeans.fut
  data/k5_n200000.in                                                    1.16x
  data/k5_n50000.in                                                     1.17x
  data/trivial.in                                                       1.26x

futhark-benchmarks/accelerate/mandelbrot/mandelbrot.fut
  #1 ("800i32 600i32 -0.7f32 0.0f32 3.067f32 100i32 16.0f...")          0.99x
  #2 ("1000i32 1000i32 -0.7f32 0.0f32 3.067f32 100i32 16....")          0.99x
  #3 ("2000i32 2000i32 -0.7f32 0.0f32 3.067f32 100i32 16....")          1.00x
  #4 ("4000i32 4000i32 -0.7f32 0.0f32 3.067f32 100i32 16....")          1.00x
  #5 ("8000i32 8000i32 -0.7f32 0.0f32 3.067f32 100i32 16....")          1.00x

futhark-benchmarks/accelerate/nbody/nbody.fut
  data/1000-bodies.in                                                   1.00x
  data/10000-bodies.in                                                  1.01x
  data/100000-bodies.in                                                 0.99x

futhark-benchmarks/accelerate/pagerank/pagerank.fut
  data/random_medium.in                                                 1.03x
  data/small.in                                                         1.19x

futhark-benchmarks/accelerate/ray/trace.fut
  #1 ("800i32 600i32 100i32 50.0f32 -100.0f32 -700.0f32 1...")          0.99x

futhark-benchmarks/accelerate/tunnel/tunnel.fut
  #1 ("10.0f32 800i32 600i32")                                          1.00x
  #2 ("10.0f32 1000i32 1000i32")                                        1.00x
  #3 ("10.0f32 2000i32 2000i32")                                        1.00x
  #4 ("10.0f32 4000i32 4000i32")                                        1.01x
  #5 ("10.0f32 8000i32 8000i32")                                        1.00x

futhark-benchmarks/finpar/LocVolCalib.fut
  LocVolCalib-data/large.in                                             1.08x
  LocVolCalib-data/medium.in                                            1.08x
  LocVolCalib-data/small.in                                             1.05x

futhark-benchmarks/finpar/OptionPricing.fut
  OptionPricing-data/large.in                                           1.10x
  OptionPricing-data/medium.in                                          1.17x
  OptionPricing-data/small.in                                           1.23x

futhark-benchmarks/jgf/crypt/crypt.fut
  crypt-data/medium.in                                                  1.10x

futhark-benchmarks/jgf/crypt/keys.fut
  crypt-data/userkey0.txt                                               1.06x

futhark-benchmarks/jgf/series/series.fut
  data/10000.in                                                         0.98x
  data/100000.in                                                        1.00x
  data/1000000.in                                                       1.01x

futhark-benchmarks/misc/bfast/bfast.fut
  data/sahara.in                                                        1.36x

futhark-benchmarks/misc/heston/heston32.fut
  data/100000_quotes.in                                                 1.02x
  data/10000_quotes.in                                                  1.02x
  data/1062_quotes.in                                                   1.07x

futhark-benchmarks/misc/heston/heston64.fut
  data/100000_quotes.in                                                 1.01x
  data/10000_quotes.in                                                  1.01x
  data/1062_quotes.in                                                   1.02x

futhark-benchmarks/misc/life/life.fut

futhark-benchmarks/misc/radix_sort/radix_sort_blelloch_benchmark.fut
  data/radix_sort_100K.in                                               0.97x
  data/radix_sort_10K.in                                                1.07x
  data/radix_sort_1M.in                                                 1.04x

futhark-benchmarks/misc/radix_sort/radix_sort_large.fut
  data/radix_sort_100K.in                                               0.99x
  data/radix_sort_10K.in                                                1.21x
  data/radix_sort_1M.in                                                 1.12x

futhark-benchmarks/parboil/mri-q/mri-q.fut
  data/large.in                                                         1.00x
  data/small.in                                                         0.99x

futhark-benchmarks/parboil/sgemm/sgemm.fut
  data/medium.in                                                        1.01x
  data/small.in                                                         1.00x
  data/tiny.in                                                          0.99x

futhark-benchmarks/parboil/stencil/stencil.fut
  data/default.in                                                       0.99x
  data/small.in                                                         0.99x

futhark-benchmarks/parboil/tpacf/tpacf.fut
  data/large.in                                                         1.00x
  data/medium.in                                                        1.01x
  data/small.in                                                         1.06x

futhark-benchmarks/rodinia/backprop/backprop.fut
  data/medium.in                                                        1.02x
  data/small.in                                                         1.52x

futhark-benchmarks/rodinia/bfs/bfs_asympt_ok_but_slow.fut
  data/4096nodes.in                                                     1.64x
  data/512nodes_high_edge_variance.in                                   1.42x
  data/64kn_32e-var-1-256-skew.in                                       1.35x
  data/graph1MW_6.in                                                    1.33x

futhark-benchmarks/rodinia/bfs/bfs_filt_padded_fused.fut
  data/4096nodes.in                                                     1.13x
  data/512nodes_high_edge_variance.in                                   0.96x
  data/64kn_32e-var-1-256-skew.in                                       1.00x
  data/graph1MW_6.in                                                    1.02x

futhark-benchmarks/rodinia/bfs/bfs_heuristic.fut
  data/4096nodes.in                                                     0.99x
  data/512nodes_high_edge_variance.in                                   0.97x
  data/64kn_32e-var-1-256-skew.in                                       1.12x
  data/graph1MW_6.in                                                    1.02x

futhark-benchmarks/rodinia/bfs/bfs_iter_work_ok.fut
  data/4096nodes.in                                                     1.09x
  data/512nodes_high_edge_variance.in                                   1.10x
  data/64kn_32e-var-1-256-skew.in                                       1.04x
  data/graph1MW_6.in                                                    1.02x

futhark-benchmarks/rodinia/cfd/cfd.fut
  data/fvcorr.domn.097K.toa                                             1.03x
  data/fvcorr.domn.193K.toa                                             1.03x

futhark-benchmarks/rodinia/hotspot/hotspot.fut
  data/1024.in                                                          1.03x
  data/512.in                                                           0.99x
  data/64.in                                                            0.55x

futhark-benchmarks/rodinia/kmeans/kmeans.fut
  data/100.in                                                           1.17x
  data/204800.in                                                        1.18x
  data/kdd_cup.in                                                       1.51x

futhark-benchmarks/rodinia/lavaMD/lavaMD.fut
  data/10_boxes.in                                                      1.00x
  data/3_boxes.in                                                       1.01x

futhark-benchmarks/rodinia/lud/lud-clean.fut
  data/16by16.in                                                        0.90x
  data/2048.in                                                          1.21x
  data/256.in                                                           1.02x
  data/512.in                                                           1.07x
  data/64.in                                                            1.10x

futhark-benchmarks/rodinia/lud/lud.fut
  data/16by16.in                                                        0.96x
  data/2048.in                                                          0.83x
  data/256.in                                                           0.95x
  data/512.in                                                           0.95x
  data/64.in                                                            0.96x

futhark-benchmarks/rodinia/myocyte/myocyte.fut
  data/medium.in                                                        1.55x
  data/small.in                                                         0.99x

futhark-benchmarks/rodinia/nn/nn.fut
  data/medium.in                                                        1.06x

futhark-benchmarks/rodinia/nw/nw.fut
  data/large.in                                                         0.86x

futhark-benchmarks/rodinia/particlefilter/particlefilter.fut
  data/128_128_10_image_10000_particles.in                              1.02x
  data/128_128_10_image_400000_particles.in                             1.02x

futhark-benchmarks/rodinia/pathfinder/pathfinder.fut
  data/medium.in                                                        1.00x

futhark-benchmarks/rodinia/srad/srad.fut
  data/image.in                                                         1.03x
```
