#!/usr/bin/env bash

################################################################################
##
## (c) 2019 SPISE MISU ApS, opensource.org/licenses/ISC
##
################################################################################

clear

stack clean

find ./bin -name "cmp-bench-json" -delete -print
find .     -name "*.cabal"        -delete -print
find .     -name ".stack-work"    -type d -exec rm -rv "{}" \;
